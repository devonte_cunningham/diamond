Hello Devonte
Your branch name is devonte.
After you clone the git repo onto your desktop, make sure to checkout your branch

git checkout devonte

After checking out your branch you can work on whatever you want. When you are done, do this

git status

This will show every change you have made
Then

git add .

This will add all of the files you changed
Then

git commit -m "WHATEVER MESSAGE"

This will commit those changes to the repo, aka making them packaged up and ready to go. Make message meaningful
Then

git push origin devonte

This will push your changes to the repo and then you are done.


When you come back to all of this next time, assuming you are only one working on your branch, you can continue
to work on stuff and then add,commit,push

But just incase your branch has been touched, which we will assume it was. Everytime you come back to Git,

git pull origin devonte

This will update your branch just incase things changed. You should always do this just to make sure.