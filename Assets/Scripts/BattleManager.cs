﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleManager : MonoBehaviour {
	public int enemy_health;
	public int baseEnemyHealth;
	public int baseHealth;
	public int attackDamage;
	public int health;
	public GameObject monster;
	private Sprite monsterSprite;
	public Sprite redmonster;
	public Sprite greenmonster;
	public Sprite bluemonster;
	public Sprite yellowmonster;
	public Sprite purplemonster;
	public Sprite yellowbubble;
	public Sprite purplebubble;
	public Sprite redbubble;
	public Sprite greenbubble;
	public Sprite bluebubble;
	public GameObject cannon;
	private bool moveToAttack = false;
	private bool moveToHome = false;
	public int tbReady;
	public int turns;
	private int originalTurns;
	public bool stopGame;
	public System.Collections.Generic.List<Sprite> spriteList = new System.Collections.Generic.List<Sprite>();
	Vector3 monsterPosition;
	public string gameMode;
	public int round;
	public GameObject resultCanvas;
	public Button continueButton;
	public Button levelSelectButton;
	public Button RestartButton;
	public Text resultsText;
	public bool popupOn;
	public GameObject Popup1;
	public GameObject EnemyHealthBar;
	public GameObject myHealthBar;
	public GameObject topWall;
	private float attackNowTime;
	private int val;


	// Use this for initialization
	void Start () {
		val = UnityEngine.Random.Range (4,11);
		attackNowTime = Time.time;
		gameMode = PlayerPrefs.GetString ("mode");
		Debug.Log (gameMode);
		if (PlayerPrefs.GetString ("mode") == "survival") {
			attackDamage = UnityEngine.Random.Range (2, 6);
			turns = UnityEngine.Random.Range (2, 5);
			spriteList.Add (redmonster);
			spriteList.Add (bluemonster);
			spriteList.Add (greenmonster);
			spriteList.Add (yellowmonster);
			spriteList.Add (purplemonster); 
			monster.GetComponent<SpriteRenderer> ().sprite= spriteList [UnityEngine.Random.Range (0, 5)];
			health = 25;
			baseHealth = health;
			enemy_health = UnityEngine.Random.Range (30,46);
			baseEnemyHealth = enemy_health;
		}

		originalTurns = turns;
		monsterPosition = monster.transform.position;
		stopGame = false;
		tbReady = 0;
		/*spriteList.Add (redmonster);
		spriteList.Add (bluemonster);
		spriteList.Add (greenmonster);
		spriteList.Add (yellowmonster);
		spriteList.Add (purplemonster); FOR OTHER GAME MODE*/
		//monster.GetComponent<SpriteRenderer> ().sprite= spriteList [UnityEngine.Random.Range (0, 5)];
		monsterSprite = monster.GetComponent<SpriteRenderer> ().sprite;
		//health = 25;
	
		//enemy_health = UnityEngine.Random.Range (30,46);



	}


	
	// Update is called once per frame
	void LateUpdate () {
		if ((Time.time - attackNowTime) > val) {
			Debug.Log (val);
			val = UnityEngine.Random.Range (5,10);
			attackNowTime = Time.time;
			moveToAttack = true;
			
		}
		if (stopGame == false) {
			if (enemy_health <= 0) {
				resultsText.text = "YOU WIN!";
				stopGame = true;
				StartCoroutine (GameOver ("winner"));

			} else {

			}

			if (health <= 0) {
				resultsText.text = "YOU LOSE!";
				stopGame = true;
				StartCoroutine (GameOver ("loser"));
			} else {

			}
		}
		if (moveToAttack && !stopGame) {
			//topWall.GetComponent<MeshRenderer>().enabled = false;
			float step = 20 * Time.deltaTime;
			monster.transform.position = Vector3.MoveTowards (monster.transform.position, cannon.transform.position, step);
			if(monster.transform.position == cannon.transform.position){
				myHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseHealth,attackDamage);
				health -= attackDamage;
				turns = originalTurns;
				moveToAttack = false;
				moveToHome = true;
			}
		}
		if (moveToHome) {
			float step = 20 * Time.deltaTime;
			monster.transform.position = Vector3.MoveTowards(monster.transform.position,monsterPosition,step);
			if(monster.transform.position == monsterPosition){
				moveToHome = false;
				//topWall.GetComponent<MeshRenderer>().enabled = true;
				cannon.GetComponent<CannonScript> ().canShoot = true;
			}

			if(popupOn == true){
				Popup1.GetComponent<PopupScript>().createPopup(2);
				popupOn = false;
			}
		}
	}

	public void enemyAttack(){
			cannon.GetComponent<CannonScript> ().canShoot = true;

	
	}

	public string determineWeakness(){
		if (monsterSprite == redmonster) {
			return "Blue";
		} else if (monsterSprite == bluemonster) {
			return "Green";
		} else if (monsterSprite == greenmonster) {
			return "Red";
		} else if (monsterSprite == yellowmonster) {
			return "Purple";
		} else if (monsterSprite == purplemonster) {
			return "Yellow";
		} else {
			return "Null";
		}

	}

	public void RestartGame(){
		StartCoroutine (GameOver ("level"));
	}


	IEnumerator GameOver(string result){
		cannon.GetComponent<CannonScript> ().canShoot = false;
		yield return new WaitForSeconds (0);
		resultCanvas.SetActive (true);
		if (result == "winner") {
			if (gameMode == "story") {
				continueButton.gameObject.SetActive(true);
				RestartButton.gameObject.SetActive(false);
				levelSelectButton.gameObject.SetActive(false);
				//Application.LoadLevel (0); //will load next level when we have more levels
			} else {
				PlayerPrefs.SetInt ("round", round + 1);
				Application.LoadLevel (Application.loadedLevel);
			}
		} else {
			if (gameMode == "story"){
				continueButton.gameObject.SetActive(false);
				RestartButton.gameObject.SetActive(true);
				levelSelectButton.gameObject.SetActive(true);
			}
				//Application.LoadLevel (0); //should return to level select when we have more levels
			else{
				PlayerPrefs.SetInt("round", 1);
				Application.LoadLevel (Application.loadedLevel);
			}
		}
	}
	public void determineDamage(GameObject go){
		if(monsterSprite.Equals(bluemonster)){
			if (go.GetComponent<SpriteRenderer> ().sprite ==greenbubble ) {
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,2);
				enemy_health -= 2;
			}
			else{
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,1);
				enemy_health -= 1;
			}
		}
		if(monsterSprite.Equals(greenmonster)){
			if (go.GetComponent<SpriteRenderer> ().sprite ==redbubble ) {
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,2);
				enemy_health -= 2;
			}
			else{
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,1);
				enemy_health -= 1;
			}
		}
		if(monsterSprite.Equals(redmonster)){
			if (go.GetComponent<SpriteRenderer> ().sprite ==bluebubble ) {
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,2);
				enemy_health -= 2;
			}
			else{
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,1);
				enemy_health -= 1;
			}
		}
		if(monsterSprite.Equals(yellowmonster)){
			if (go.GetComponent<SpriteRenderer> ().sprite ==purplebubble ) {
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,2);
				enemy_health -= 2;
			}
			else{
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,1);
				enemy_health -= 1;
			}
		}
		if(monsterSprite.Equals(purplemonster)){
			if (go.GetComponent<SpriteRenderer> ().sprite ==yellowbubble ) {
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,2);
				enemy_health -= 2;
			}
			else{
				EnemyHealthBar.GetComponent<ProgressBar>().decreaseHealth(baseEnemyHealth,1);
				enemy_health -= 1;
			}
		}








	}




}
