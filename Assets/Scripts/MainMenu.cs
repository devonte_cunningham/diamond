﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	void OnGUI(){
		if (GUI.Button (new Rect (Screen.width * .25f, Screen.height * .2f,
		                     Screen.width * .5f, Screen.height * .2f),
		            "Story Mode")) {
			Debug.Log ("play game");
		}

		if (GUI.Button (new Rect (Screen.width * .25f, Screen.height * .5f,
		                          Screen.width * .5f, Screen.height * .2f),
		                "Survival Mode")) {
			Application.LoadLevel("level");
			Debug.Log ("Survive");
		}
	}
}
