﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PopupScript : MonoBehaviour {
	public string information;
	public string information2;
	public Text InfoText;
	//public bool fullyClosed;


	public void closePopup(){
		StartCoroutine (popupClosed ());

	
	}


	public void createPopup(int popupNumber){
		//fullyClosed = false;
		switch (popupNumber) {
		case 1:
			InfoText.text = information;
			break;
		case 2: 
			InfoText.text = information2;
			break;
		}
		this.gameObject.SetActive (true);
	}

	IEnumerator popupClosed(){
		yield return new WaitForSeconds(0.2f);
		//fullyClosed = true;
		this.gameObject.SetActive (false);

	}
}
