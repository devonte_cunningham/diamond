﻿using UnityEngine;
using System.Collections;

public class PanelOptions : MonoBehaviour {


	public void restartLevel(){
		Application.LoadLevel (Application.loadedLevel);
	}

	public void continueGame(){
		string levelName = Application.loadedLevelName.Substring (5);
		Debug.Log ("LEVELNAME:" + levelName);
		int level = int.Parse (levelName);
		if (level > PlayerPrefs.GetInt ("unlockLevel")) {
			PlayerPrefs.SetInt("unlockLevel",level);
		}
		Application.LoadLevel ("NewLevelSelect");
	}

	public void levelSelect(){
		Application.LoadLevel ("NewLevelSelect");
	}
}
