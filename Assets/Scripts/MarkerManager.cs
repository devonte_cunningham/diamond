﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MarkerManager : MonoBehaviour {
	public GameObject canvas;
	public GameObject playButton;
	public Text LevelText;
	private string levelNumber;
	public GameObject marker;
	public GameObject lockedManager;
	public GameObject levelLoader;
	private bool arrived = false;
	public System.Collections.Generic.List<GameObject> levelMarkers = new System.Collections.Generic.List<GameObject>();
	private System.Collections.Generic.List<GameObject> lockedLevelMarkers;

	public void Start(){
		int endIndex = name.IndexOf ("M");
		int startIndex = 5;
		int length = endIndex - startIndex;
		levelNumber = name.Substring (startIndex,length);



		lockedLevelMarkers = lockedManager.GetComponent<LockedLevelsManager> ().lockedLevelMarkers;
		foreach(GameObject g in GameObject.FindObjectsOfType(typeof(GameObject))){
			if(g.name.EndsWith("Marker") && g.name != name){
				levelMarkers.Add(g);
			}
		}
	}
	private void OnMouseDown(){
		if (!lockedLevelMarkers.Contains (this.gameObject)) {
			levelLoader.GetComponent<LevelLoader> ().PickLevel (levelNumber);
			canvas.SetActive(false);
			arrived = true;
			marker.GetComponent<SpriteRenderer> ().enabled = false;
			foreach (GameObject g in levelMarkers) {
				if (g.name.EndsWith ("Marker") && g.name != name) {
					g.SetActive (false);
				}
			}

		}


	}

	public void Update(){
		if (arrived) {
			float step = 5* Time.deltaTime;
			marker.transform.position = Vector3.MoveTowards (marker.transform.position, transform.position, step);

		}

		if(marker.transform.position == transform.position){
			foreach(GameObject g in levelMarkers){
				if(g.name.EndsWith("Marker") && g.name != name){
					g.SetActive(true);
				}
			}
			marker.GetComponent<SpriteRenderer> ().enabled = true;
			arrived = false;
			canvas.SetActive(true);
			LevelText.text = "Level " + levelNumber;
		}


	}
}
