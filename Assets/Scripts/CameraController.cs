﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public GameObject marker;

	// Update is called once per frame
	void LateUpdate () {
		transform.position = new Vector3(marker.transform.position.x,marker.transform.position.y,transform.position.z);
	}
}
