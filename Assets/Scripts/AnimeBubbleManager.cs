﻿using UnityEngine;
using System.Collections;

public class AnimeBubbleManager : MonoBehaviour {
	public GameObject cannon;
	public GameObject battleManager;
	public GameObject monster;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (gameObject.name == "animeBubbleSpawn") {
			float step = 20* Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, monster.transform.position, step);
			if (transform.position == monster.transform.position) {
				Debug.Log("At monster");
				battleManager.GetComponent<BattleManager>().determineDamage(gameObject);
				Destroy(gameObject);
			}
		}
		
		
	}
}
