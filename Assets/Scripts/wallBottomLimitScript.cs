﻿using UnityEngine;
using System.Collections;

public class wallBottomLimitScript : MonoBehaviour {
	public GameObject BattleManager;
	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.CompareTag("bubbles")) {
			BattleManager.GetComponent<BattleManager>().RestartGame();
		} 
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
