﻿using UnityEngine;
using System.Collections;

public class StaticBubbleManager : MonoBehaviour {

	public System.Collections.Generic.HashSet<GameObject> neighboringBubbles = new System.Collections.Generic.HashSet<GameObject>();
	public System.Collections.Generic.HashSet<GameObject> neighboringBubbles2;
	public System.Collections.Generic.HashSet<GameObject> connectedBubbleList = new System.Collections.Generic.HashSet<GameObject>();
	private int stopCycle = 0;
	private int stopGenerating = 0;
	private Sprite spriteColor; 
	public Sprite redbubble;
	public Sprite bluebubble;
	public Sprite greenbubble;
	public Sprite yellowbubble;
	public Sprite purplebubble;
	public System.Collections.Generic.List<Sprite> spriteList = new System.Collections.Generic.List<Sprite>();

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.GetString ("mode") == "survival") {
		spriteList.Add (redbubble);
		spriteList.Add (bluebubble);
		spriteList.Add (greenbubble);
		spriteList.Add (yellowbubble);
		spriteList.Add (purplebubble);
		GetComponent<SpriteRenderer> ().sprite = GenerateRandom ();
		}

		spriteColor = GetComponent<SpriteRenderer> ().sprite;

		foreach (GameObject stat in GameObject.FindObjectsOfType(typeof(GameObject))) {
			if(stat.name.Contains("static") && stat.transform.position.y > 8.7){
				stat.GetComponent<SpriteRenderer>().enabled = false;
			}
		}
	


	}

	// Update is called once per frame
	void Update () {
		if (stopGenerating == 0) {
			GenerateConnected (this.gameObject);
			while (stopCycle != 0) {
				neighboringBubbles2 = new System.Collections.Generic.HashSet<GameObject> (neighboringBubbles);
				foreach (GameObject n in neighboringBubbles2) {
					GenerateConnected (n);
					neighboringBubbles.Remove (n);
				}
				stopCycle -= 1;
			}
			stopGenerating = 1;
		}
	}

	void GenerateConnected(GameObject bubble){
		foreach (GameObject go in GameObject.FindObjectsOfType(typeof(GameObject))) {
			if(go.name.Contains("static")){
				float value = Mathf.Sqrt (Mathf.Pow ((bubble.transform.position.x - go.transform.position.x), 2) + Mathf.Pow ((bubble.transform.position.y - go.transform.position.y), 2));
				
				//Distance between a specific bubble that accounts for the range of all neighboring bubbles.
				if (value > 0.74f && value < 0.82f && !connectedBubbleList.Contains(go) && go.GetComponent<SpriteRenderer>().sprite.Equals(spriteColor)) {
					//Debug.Log (go.name + " " + name);
					neighboringBubbles.Add(go);
					connectedBubbleList.Add(go);
					stopCycle += 1;
				}
				
				
			}
		}
	}

	public Sprite GenerateRandom(){
		return spriteList [UnityEngine.Random.Range (0, 5)];
	}
}
