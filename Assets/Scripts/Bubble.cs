﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class Bubble : MonoBehaviour {

	public GameObject gridManager;
	public GameObject static_bubble;
	public Transform wall_top;
	public GameObject grid_bubble;
	public GameObject testBubble;
	private GameObject testBubbleInstance;
	public GameObject battleManager;
	public  System.Collections.Generic.HashSet<GameObject> gridBubbles;
	public System.Collections.Generic.List<GameObject> neighboringBubbles = new System.Collections.Generic.List<GameObject>();
	public System.Collections.Generic.HashSet<GameObject> gridBubbleList = new System.Collections.Generic.HashSet<GameObject>();
	public System.Collections.Generic.HashSet<GameObject> connectedBubbleList = new System.Collections.Generic.HashSet<GameObject>();

	
	private GridBubbleManager gridBubbleManager;
	private Sprite spriteColor;
	private GameObject returnGridBubble;


	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.name.Contains ("static_bubble") || col.gameObject.name == "testBubble(Clone)") {
			gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;

			foreach(GameObject g in gridBubbles){
				float value = Mathf.Sqrt (Mathf.Pow ((col.gameObject.transform.position.x - g.transform.position.x), 2) + Mathf.Pow ((col.gameObject.transform.position.y - g.transform.position.y), 2));
				
				//Distance between a specific bubble that accounts for the range of all neighboring bubbles.
				if (value > 0.74f && value < 0.82f) {
					neighboringBubbles.Add (g);
				}
			}
		
			grid_bubble = placement (transform.position.x, transform.position.y); //assigns to the chosen gridBubble
			Debug.Log ("GridBubble: " + grid_bubble.transform.position.y);
			if(grid_bubble.transform.position.y < 1.75f){
				battleManager.GetComponent<BattleManager>().health = 0;
			}
			gridBubbleManager.gridBubbleList.Remove(grid_bubble); //States that the gridBubble chosen is now occupied and can no longer be assigned.
			Debug.Log ("REMOVED: " + grid_bubble.name);
			gameObject.transform.position = new Vector3 (grid_bubble.transform.position.x, grid_bubble.transform.position.y, grid_bubble.transform.position.z);
			testBubbleInstance = Instantiate(testBubble,gameObject.transform.position,gameObject.transform.rotation) as GameObject;
			testBubbleInstance.transform.parent = wall_top;
			testBubbleInstance.GetComponent<SpriteRenderer>().sprite = spriteColor;
			testBubbleInstance.GetComponent<TestBubble>().gridBubble = grid_bubble;
			Destroy (gameObject);
			/*
			foreach(GameObject go in GameObject.FindObjectsOfType(typeof(GameObject))){
				if(go.name.Contains("static_bubble") || go.name.Contains ("bubble(Clone)")){
					float colValue = Mathf.Sqrt (Mathf.Pow ((transform.position.x - go.transform.position.x), 2) + Mathf.Pow ((transform.position.y - go.transform.position.y), 2));
					if(colValue > 0.74f && colValue < 0.82f){

	
						if(go.gameObject.GetComponent<SpriteRenderer>().sprite.Equals(spriteColor)){

							if(go.gameObject.GetComponent<StaticBubbleManager>().connectedBubbleList.Count >= 2){
								Destroy (gameObject);
								gridBubbles.Add (grid_bubble);
								foreach(GameObject cb in go.gameObject.GetComponent<StaticBubbleManager>().connectedBubbleList){
									
									returnGridBubble = gridPlacement (cb.transform.position.x,cb.transform.position.y);
									
									gridBubbles.Add (returnGridBubble);
									Destroy (cb);
								}
							

							

							}
							else{
			
								connectedBubbleList.Add(gameObject);
								go.gameObject.GetComponent<StaticBubbleManager>().connectedBubbleList.Add (gameObject);
								go.gameObject.GetComponent<StaticBubbleManager>().connectedBubbleList.Add (go);
							}
						}
					}
				}

			}



			*/
			//Game Resets if there is a bubble on the bottom row (Better than destroying everything lol)

		}
	}

	//Load gridBubble Manager Script and use its gridBubbleList
	void Start (){
		Screen.orientation = ScreenOrientation.Portrait; // just for play test

		spriteColor = GetComponent<SpriteRenderer> ().sprite;
		gridBubbleManager = gridManager.GetComponent<GridBubbleManager> ();
		gridBubbles = gridBubbleManager.gridBubbleList;

		foreach (GameObject go in GameObject.FindObjectsOfType(typeof(GameObject))) {
			if(go.name.Contains("grid")){
				gridBubbleList.Add(go);
				
			}
		}
	}


	
	// Update is called once per frame
	void Update () {
		if (gameObject.name.Contains("pseudoBubble(Clone)")){

			//Vector3 originalCoords = new Vector3(3,3,0);

			Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Vector2 origin = new Vector2(0,0);
			Vector2 horizontal = new Vector2(1,0);

			float angle = Vector2.Angle(origin-horizontal,origin-mousePos);
			//Debug.Log(angle);

			//The degrees of freedom we are giving the cannon when angling its aim.
			if (angle >= 40 && angle <= 143 && mousePos.y > -9) {
				angle = angle/59 ;
				Vector2 temp = origin;
				temp.x += Mathf.Cos (angle);
				temp.y += Mathf.Sin (angle)+.5f;

				transform.position = temp;
			}
		}
		
		
		if(gameObject.transform.position.y < -4 && gameObject.name == "bubble(Clone)"){
			Destroy(gameObject);
		}
	}
	

	//This function returns the gridbubble closest to the object given its x and y position.
	GameObject placement(float x,float y){
		int chosenBubble = 0;
		float minimum = 500.0f;
		for(int i = 0;i < neighboringBubbles.Count;i++){
			float value = Mathf.Sqrt(Mathf.Pow ((x - neighboringBubbles[i].transform.position.x),2) + Mathf.Pow ((y - neighboringBubbles[i].transform.position.y),2));

			if(value < minimum){
				minimum = value;
				chosenBubble = i;
			}
		}
		return neighboringBubbles[chosenBubble];
	}

	GameObject gridPlacement(float x,float y){
		GameObject chosenBubble = gameObject;
		float minimum = 500.0f;
		foreach (GameObject g in gridBubbleList) {
			float value = Mathf.Sqrt(Mathf.Pow ((x - g.transform.position.x),2) + Mathf.Pow ((y - g.transform.position.y),2));
			
			if(value < minimum){
				minimum = value;
				chosenBubble = g;
			}
		}

		return chosenBubble;
	}






}