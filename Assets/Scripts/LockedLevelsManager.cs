﻿using UnityEngine;
using System.Collections;

public class LockedLevelsManager : MonoBehaviour {
	public System.Collections.Generic.List<GameObject> lockedLevelMarkers = new System.Collections.Generic.List<GameObject>();
	public System.Collections.Generic.HashSet<GameObject> levelNames= new System.Collections.Generic.HashSet<GameObject>();
	public GameObject marker;
	// Use this for initialization
	void Start () {
		foreach(GameObject g in GameObject.FindObjectsOfType(typeof(GameObject))){
			if(g.name.EndsWith("Marker") && g.name != name){
				lockedLevelMarkers.Add(g);
				Debug.Log (g.name);
			}
		}
		lockedLevelMarkers.Sort (SortByName);
		lockedLevelMarkers.RemoveAt(0);
		//PlayerPrefs.SetInt ("unlockLevel", 0);
		for(int i = 0; i < PlayerPrefs.GetInt("unlockLevel");i++){
			lockedLevelMarkers.RemoveAt (0);
		}
		marker.transform.position = GameObject.Find ("Level" + PlayerPrefs.GetInt ("unlockLevel") + "Marker").transform.position;

	}

	private static int SortByName(GameObject obj1, GameObject obj2){
		return obj1.name.CompareTo (obj2.name);
	}
	// Update is called once per frame
	void Update () {

	}

	void unlockLevel(GameObject level){
		lockedLevelMarkers.Remove (level);
	}

	public void ResetGame(){
		PlayerPrefs.SetInt ("unlockLevel", 0);

	}
}
