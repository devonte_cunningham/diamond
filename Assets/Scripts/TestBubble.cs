﻿using UnityEngine;
using System.Collections;

public class TestBubble : MonoBehaviour {
	private GridBubbleManager gridBubbleManager;
	public GameObject monster;
	public GameObject cannon;
	public GameObject animeBubble;
	private GameObject animeBubbleInstance;
	private Sprite spriteColor;
	public GameObject gridManager;
	public System.Collections.Generic.HashSet<GameObject> gridBubbles;
	public System.Collections.Generic.HashSet<GameObject>gridBubbleList = new System.Collections.Generic.HashSet<GameObject>();
	public GameObject gridBubble;
	public GameObject battleManager;
	private GameObject returnGridBubble;
	public System.Collections.Generic.HashSet<GameObject> connectedBubbleList = new System.Collections.Generic.HashSet<GameObject>();
	public System.Collections.Generic.HashSet<GameObject> upperList = new System.Collections.Generic.HashSet<GameObject>();

	public bool signal = false;

	// Use this for initialization
	void Start () {





		spriteColor = GetComponent<SpriteRenderer> ().sprite;
		gridBubbleManager = gridManager.GetComponent<GridBubbleManager> ();
		gridBubbles = gridBubbleManager.gridBubbleList;
		gameObject.GetComponent<Rigidbody2D> ().isKinematic = true;
		foreach (GameObject go in GameObject.FindObjectsOfType(typeof(GameObject))) {
			if(go.name.Contains("grid")){
				gridBubbleList.Add(go);
				
			}
		}


		foreach(GameObject go in GameObject.FindObjectsOfType(typeof(GameObject))){
			if(go.name.Contains("static_bubble")){
				float colValue = Mathf.Sqrt (Mathf.Pow ((transform.position.x - go.transform.position.x), 2) + Mathf.Pow ((transform.position.y - go.transform.position.y), 2));
				if(colValue > 0.74f && colValue < 0.82f){

					if(go.gameObject.GetComponent<SpriteRenderer>().sprite.Equals(spriteColor)){
						
						if(go.gameObject.GetComponent<StaticBubbleManager>().connectedBubbleList.Count >= 2){
							//destroyList.Add (gameObject);
							//Destroy (gameObject);
							connectedBubbleList.Add (gameObject);

							foreach(GameObject cb in go.gameObject.GetComponent<StaticBubbleManager>().connectedBubbleList){
								

								connectedBubbleList.Add (cb);

							}
							
							
							
							
						}
						else{
							connectedBubbleList.Add (go);
							connectedBubbleList.Add(gameObject);
							go.gameObject.GetComponent<StaticBubbleManager>().connectedBubbleList.Add (gameObject);
							go.gameObject.GetComponent<StaticBubbleManager>().connectedBubbleList.Add (go);
						}
					}
				}
			}
			if(go.name.Contains("testBubble(Clone)")){
				float colValue = Mathf.Sqrt (Mathf.Pow ((transform.position.x - go.transform.position.x), 2) + Mathf.Pow ((transform.position.y - go.transform.position.y), 2));
				if(colValue > 0.74f && colValue < 0.82f){

					if(go.gameObject.GetComponent<SpriteRenderer>().sprite.Equals(spriteColor)){

						if(go.gameObject.GetComponent<TestBubble>().connectedBubbleList.Count >= 2){

							//Destroy (gameObject);
							connectedBubbleList.Add (gameObject);


							foreach(GameObject cb in go.gameObject.GetComponent<TestBubble>().connectedBubbleList){
								
							
								//Destroy (cb);
								connectedBubbleList.Add (cb);
							}
							
							
							
							
						}
						else{

							connectedBubbleList.Add(gameObject);
							connectedBubbleList.Add (go);
							go.gameObject.GetComponent<TestBubble>().connectedBubbleList.Add (gameObject);
							go.gameObject.GetComponent<TestBubble>().connectedBubbleList.Add (go);
						}
					}
				}
			}
			
		}
		if (connectedBubbleList.Count >= 3) {
			foreach(GameObject b in connectedBubbleList){
				returnGridBubble = gridPlacement (b.transform.position.x,b.transform.position.y);
				gridBubbles.Add (returnGridBubble);

				animeBubbleInstance = Instantiate(animeBubble,b.transform.position,b.transform.rotation) as GameObject;
				animeBubbleInstance.gameObject.GetComponent<SpriteRenderer>().sprite = b.GetComponent<SpriteRenderer>().sprite;
				animeBubbleInstance.gameObject.name = "animeBubbleSpawn";
				Destroy (b);
				//signal = true;

				//float step = 10* Time.deltaTime;
				//b.transform.position = Vector3.MoveTowards(b.transform.position, cannon.transform.position, step);
				//b.transform.position = new Vector3(0.0f,cannon.transform.position.y,0.0f);
				//Debug.Log (b.name + "POS: " + b.transform.position.x);
				//Destroy (b);
		

			}
			cannon.GetComponent<CannonScript>().doIt = 1;

		}



			
		

	}


	
	// Update is called once per frame
	void Update () {

		/*if (signal) {

			foreach(GameObject b in connectedBubbleList){
				float step = 8* Time.deltaTime;
				b.transform.position = Vector3.MoveTowards(b.transform.position, cannon.transform.position, step);
				if(b.transform.position == cannon.transform.position){
					animationBubbleList.Add(b);
				}
				
				
			}
			if(connectedBubbleList.Count == animationBubbleList.Count){
				foreach(GameObject b in connectedBubbleList){
					battleManager.GetComponent<BattleManager>().determineDamage(b);
					Destroy(b);

				}
				animationBubbleList.Clear();
				signal = false;
			}

		}*/
	
	}




	GameObject gridPlacement(float x,float y){
		GameObject chosenBubble = gameObject;
		float minimum = 500.0f;
		foreach (GameObject g in gridBubbleList) {
			float value = Mathf.Sqrt(Mathf.Pow ((x - g.transform.position.x),2) + Mathf.Pow ((y - g.transform.position.y),2));
			
			if(value < minimum){
				minimum = value;
				chosenBubble = g;
			}
		}
		
		return chosenBubble;
	}

	public void connectedToTop(){
		foreach(GameObject go in GameObject.FindObjectsOfType(typeof(GameObject))){
			if(go.name.Contains("static_bubble") || go.name.Contains ("testBubble(Clone)")){
				float nValue = Mathf.Sqrt (Mathf.Pow ((transform.position.x - go.transform.position.x), 2) + Mathf.Pow ((transform.position.y - go.transform.position.y), 2));
				if(nValue > 0.74f && nValue < 0.82f){
				}
			}
		}
	}


}
