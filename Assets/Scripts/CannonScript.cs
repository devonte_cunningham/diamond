﻿using UnityEngine;
using System.Collections;
using System;



public class CannonScript : MonoBehaviour {
	public Rigidbody2D bubble;
	private Rigidbody2D instance;


	public GameObject topWall;
	public GameObject gridManager;
	private GameObject returnGridBubble;
	public GameObject switchBall;


	public GameObject psuedoBubble;
	public GameObject ShotSpawn;
	public GameObject psuedoInstance;

	public float speed = 5.0f;
	public float fireRate;
	private float nextFire;
	public float doIt = 0;
	private int stopCycle = 0;
	public bool canShoot;
	public int animAmount = 0;


	public Sprite greenBubble;
	public Sprite redBubble;
	public Sprite blueBubble;
	public Sprite yellowBubble;
	public Sprite purpleBubble;
	public GameObject battleManager;
	public GameObject monster;
	public GameObject animeBubble;
	private GameObject animeBubbleInstance;
	public GameObject resultsPanel;
	public GameObject Popup1;
	public bool popupOn;


	public GameObject StaticBubblesFolder;
	public GameObject GridBubblesFolder;


	public System.Collections.Generic.HashSet<GameObject> neighboringBubbles = new System.Collections.Generic.HashSet<GameObject>();
	public System.Collections.Generic.HashSet<GameObject> connectedBubbleList = new System.Collections.Generic.HashSet<GameObject>();
	public System.Collections.Generic.HashSet<GameObject> neighboringBubbles2 = new System.Collections.Generic.HashSet<GameObject>();
	public System.Collections.Generic.HashSet<GameObject>gridBubbleList = new System.Collections.Generic.HashSet<GameObject>();


	public System.Collections.Generic.List<Sprite> spriteList = new System.Collections.Generic.List<Sprite>();
	public System.Collections.Generic.List<Sprite> pooling = new System.Collections.Generic.List<Sprite>();
	public bool signal = false;

	public Vector3 shotSpawnPosition = new Vector3(0,-2.8f,0);


	// Use this for initialization.
	void Start () {
		canShoot = true;
		foreach (GameObject go in GameObject.FindObjectsOfType(typeof(GameObject))) {
			if(go.name.Contains("grid")){
				gridBubbleList.Add(go);
				
			}
		}
		if (PlayerPrefs.GetString ("mode") == "survival") {
			spriteList.Add (redBubble);
			spriteList.Add (greenBubble);
			spriteList.Add (blueBubble);
			spriteList.Add (yellowBubble);
			spriteList.Add (purpleBubble);
		}
		psuedoInstance = Instantiate(psuedoBubble,shotSpawnPosition,ShotSpawn.transform.rotation) as GameObject;//Fake instance of bubble being shot
		if(popupOn){
			Popup1.GetComponent<PopupScript>().createPopup(1);
		}

		int choice = UnityEngine.Random.Range(0,spriteList.Count);
		pooling.Add (spriteList [choice]);
		choice = UnityEngine.Random.Range(0,spriteList.Count);
		pooling.Add (spriteList [choice]);
		choice = UnityEngine.Random.Range(0,spriteList.Count);
		pooling.Add (spriteList [choice]);
		psuedoInstance.GetComponent<SpriteRenderer> ().sprite = pooling [0];
		bubble.GetComponent<SpriteRenderer> ().sprite = pooling [0];
		switchBall.GetComponent<switchBallScript> ().startSprite ();
		Debug.Log (pooling [0].name + " " + pooling [1].name + " " + pooling [2].name);
	}


	// Update is called once per frame
	void LateUpdate () { 


		if (doIt == 1) {
			calculateDrop();
			doIt = 0;
		}


		Vector3 mousePos = Input.mousePosition;
		//mousePos.z = 5.23f; dont think we need this line
		
		Vector3 objectPos = Camera.main.WorldToScreenPoint (transform.position);
		mousePos.x = mousePos.x - objectPos.x;
		mousePos.y = mousePos.y - objectPos.y;
		
		float angle = Mathf.Atan2 (mousePos.y, mousePos.x) * Mathf.Rad2Deg;

		//The degrees of freedom we are giving the cannon when angling its aim.
		if (angle >= 20 && angle <= 170 && mousePos.y > -9) {
			transform.rotation = Quaternion.Euler (new Vector3 (0, 0, angle));
			psuedoInstance.gameObject.transform.rotation = Quaternion.Euler (new Vector3 (0, 0, angle));
		}
		//Doesn't shoot until user lets go of click/touch
		if (Input.GetMouseButtonUp(0) && mousePos.y > -9 && canShoot && resultsPanel.activeSelf != true && Popup1.activeSelf !=true){//&& Time.time > nextFire) {
			//nextFire = Time.time + fireRate;
			Debug.Log("MOUSE POSITION: " + mousePos.x + "____" + mousePos.y);
			pooling.RemoveAt(0);
			instance = Instantiate(bubble,shotSpawnPosition,ShotSpawn.transform.rotation) as Rigidbody2D;
			instance.AddForce (800 * transform.right);
			Destroy (psuedoInstance);
			int choice = UnityEngine.Random.Range(0,spriteList.Count);
			pooling.Add (spriteList[choice]);
			bubble.GetComponent<SpriteRenderer>().sprite = pooling[0];
			psuedoBubble.GetComponent<SpriteRenderer>().sprite = pooling[0];
			psuedoInstance = Instantiate(psuedoBubble,shotSpawnPosition,ShotSpawn.transform.rotation) as GameObject;
			canShoot = false;
			switchBall.GetComponent<switchBallScript>().startSprite();
			StartCoroutine(enemyTurn());

		
		}
		float step = 0.1f * Time.deltaTime;
		topWall.transform.position = Vector3.MoveTowards (topWall.transform.position,transform.position, step);

	} 

	public void switchBalls(){
		bubble.GetComponent<SpriteRenderer>().sprite = pooling[0];
		psuedoBubble.GetComponent<SpriteRenderer>().sprite = pooling[0];
		Destroy (psuedoInstance);
		psuedoInstance = Instantiate(psuedoBubble,shotSpawnPosition,ShotSpawn.transform.rotation) as GameObject;
	}

	public void calculateDrop(){
		connectedBubbleList = new System.Collections.Generic.HashSet<GameObject>();

		System.Collections.Generic.List<GameObject> topList = new System.Collections.Generic.List<GameObject>();
		GameObject top1 = GameObject.Find ("static_bubble (165)");
		GameObject top2 = GameObject.Find ("static_bubble (166)");
		GameObject top3 = GameObject.Find ("static_bubble (167)");
		GameObject top4 = GameObject.Find ("static_bubble (168)");
		GameObject top5 = GameObject.Find ("static_bubble (169)");
		GameObject top6 = GameObject.Find ("static_bubble (170)");
		topList.Add (top1);
		topList.Add (top2);
		topList.Add (top3);
		topList.Add (top4);
		topList.Add (top5);
		topList.Add (top6);



		foreach (GameObject t in topList) {
			GenerateConnected(t);
			connectedBubbleList.Add (t);
		}
		while (stopCycle != 0) {
			neighboringBubbles2 = new System.Collections.Generic.HashSet<GameObject> (neighboringBubbles);
			foreach (GameObject n in neighboringBubbles2) {
				GenerateConnected (n);
				neighboringBubbles.Remove (n);
			}
			stopCycle -= 1;
		}




			
		foreach (GameObject go in GameObject.FindObjectsOfType(typeof(GameObject))) {
			if(go.name.Contains("static_bubble") || go.name.Contains ("testBubble(Clone)")){
				if(!connectedBubbleList.Contains(go)){
					returnGridBubble = gridPlacement(go.transform.position.x,go.transform.position.y);
					gridManager.GetComponent<GridBubbleManager> ().gridBubbleList.Add (returnGridBubble);
					//battleManager.GetComponent<BattleManager>().determineDamage(go);
					animeBubbleInstance = Instantiate(animeBubble,go.transform.position,go.transform.rotation) as GameObject;
					animeBubbleInstance.gameObject.GetComponent<SpriteRenderer>().sprite = go.GetComponent<SpriteRenderer>().sprite;
					animeBubbleInstance.gameObject.name = "animeBubbleSpawn";
					Destroy(go);
				}
			}
				
		}
	
		




	
	}

	void GenerateConnected(GameObject bubble){
		foreach (GameObject go in GameObject.FindGameObjectsWithTag("bubbles")) {
			float value = Mathf.Sqrt (Mathf.Pow ((bubble.transform.position.x - go.transform.position.x), 2) + Mathf.Pow ((bubble.transform.position.y - go.transform.position.y), 2));
				
				//Distance between a specific bubble that accounts for the range of all neighboring bubbles.
			if (value > 0.74f && value < 0.82f && !connectedBubbleList.Contains(go)) {
					//Debug.Log (go.name + " " + name);
				neighboringBubbles.Add(go);
				connectedBubbleList.Add(go);
				stopCycle += 1;

			}
				

		}
	}

	IEnumerator enemyTurn(){

		yield return new WaitForSeconds (0.5f);
		Debug.Log ("inside enemyTurn");
		battleManager.GetComponent<BattleManager> ().enemyAttack ();
		yield return new WaitForSeconds (0.2f);

	}



	GameObject gridPlacement(float x,float y){
		GameObject chosenBubble = gameObject;
		float minimum = 500.0f;
		foreach (GameObject g in gridBubbleList) {
			float value = Mathf.Sqrt(Mathf.Pow ((x - g.transform.position.x),2) + Mathf.Pow ((y - g.transform.position.y),2));
			
			if(value < minimum){
				minimum = value;
				chosenBubble = g;
			}
		}
		
		return chosenBubble;
	}

	
}
