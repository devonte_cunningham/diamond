﻿using UnityEngine;
using System.Collections;

public class ProgressBar : MonoBehaviour {
	private GUIStyle currentStyle = null;
	private GUIStyle differentStyle = null;
	public float barDisplay; //current progress
	public Vector2 pos = new Vector2(20,40);
	public Vector2 size = new Vector2(60,20);
	public Texture2D emptyTex;
	public Texture2D fullTex;
	public float damageDone;
	void OnGUI() {
		//draw the background:
		InitStyles();
		GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
		GUI.Box(new Rect(0,0, size.x, size.y), emptyTex,differentStyle);
		
		//draw the filled-in part:
		GUI.BeginGroup(new Rect(0,0, size.x * barDisplay, size.y));
		GUI.Box(new Rect(0,0, size.x, size.y), fullTex,currentStyle);
		GUI.EndGroup();
		GUI.EndGroup();

	}
	
	void Update() {
		barDisplay = 1.0f - damageDone;
	}

	public void decreaseHealth(float health, float damage){
		damageDone += ((damage/health));
	}

	private void InitStyles()
	{
		if( currentStyle == null )
		{
			currentStyle = new GUIStyle( GUI.skin.box );
			currentStyle.normal.background = MakeTex( 2, 2, new Color( 0f, 1f, 0f, 0.5f ) );
		}
		if( differentStyle == null )
		{
			differentStyle = new GUIStyle( GUI.skin.box );
			differentStyle.normal.background = MakeTex( 2, 2, new Color( 1f, 0.5f, 0f, 0.5f ) );
		}
	}
	
	private Texture2D MakeTex( int width, int height, Color col )
	{
		Color[] pix = new Color[width * height];
		for( int i = 0; i < pix.Length; ++i )
		{
			pix[ i ] = col;
		}
		Texture2D result = new Texture2D( width, height );
		result.SetPixels( pix );
		result.Apply();
		return result;
	}
}
