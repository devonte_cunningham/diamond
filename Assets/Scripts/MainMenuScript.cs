﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuScript : MonoBehaviour {
	
	public Canvas quitMenu;
	public Button startText;
	public Button exitText;

	public Canvas modeMenu;
	public Button survivalModeButton;
	public Button storyModeButton;
	public Button backButton;
	
	// Use this for initialization
	void Start () {
		Screen.orientation = ScreenOrientation.LandscapeLeft; // just for play test
		quitMenu = quitMenu.GetComponent<Canvas> ();
		exitText = exitText.GetComponent<Button>();
		startText = startText.GetComponent<Button>();
		quitMenu.enabled = false;

		modeMenu = modeMenu.GetComponent<Canvas> ();
		survivalModeButton = survivalModeButton.GetComponent<Button>();
		storyModeButton = storyModeButton.GetComponent<Button>();
		modeMenu.enabled = false;
	}
	
	public void QuitPressed(){
		quitMenu.enabled = true;
		startText.enabled = false;
		exitText.enabled = false;
	}
	public void cancelled(){
		quitMenu.enabled = false;
		startText.enabled = true;
		exitText.enabled = true;
	}
	
	public void play(){
		modeMenu.enabled = true;
		exitText.enabled = false;
	}

	public void storyMode(){
		PlayerPrefs.SetString ("mode", "story");
		Application.LoadLevel ("NewLevelSelect");
	}

	public void survivalMode(){
		PlayerPrefs.SetString ("mode", "survival");
		PlayerPrefs.SetInt ("round", 1);
		Application.LoadLevel ("Level");
	}

	public void modeMenuBack(){
		modeMenu.enabled = false;
		exitText.enabled = true;
	}

	
	public void QuitGame(){
		Application.Quit ();
	}
}